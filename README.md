# Building
```
mvn clean install -DskipTests=true -U
```

# Configuration
The KDE security realm has 4 settings. They all need to be set for the realm
to work.

1. client id: the PHID of the client id from phabricator
1. client secret: the secret key of the client from phabricator
1. query certificate: a secret key for bot account (see help on the
  security configuration page).
1. query user: the bot user name

# Authorization
The plugin currently does not implement an authorization strategy (i.e. ACL),
so a pre-existing authorization strategy like Matrix or Role-based needs to
be used.
The organizations a user is member of is reflected in Jenkins as the
organizations' PHID. To see all applicable PHIDs please visit your user
page.

# API
API access requires impersonation and that requires a reverse-lookup from
a user name to a User entity (i.e. figure out which groups the user is member
of). We currently do not implement this as it requires bot access and it is
not clear if we want to keep using bot accounts. To get admin API access you
need to make sure the relevant users are manually listed to have the relevant
permissions.
