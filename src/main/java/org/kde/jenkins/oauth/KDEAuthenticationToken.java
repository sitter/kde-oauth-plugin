/*
    Copyright (C) 2016 Harald Sitter <sitter@kde.org>

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

package org.kde.jenkins.oauth;

import at.quelltextlich.phabricator.conduit.ConduitException;
import at.quelltextlich.phabricator.conduit.raw.Conduit;
import at.quelltextlich.phabricator.conduit.raw.ConduitFactory;
import at.quelltextlich.phabricator.conduit.raw.ProjectModule;
import org.acegisecurity.GrantedAuthority;
import org.acegisecurity.GrantedAuthorityImpl;
import org.acegisecurity.providers.AbstractAuthenticationToken;
import org.apache.commons.lang.StringUtils;
import org.kde.jenkins.oauth.api.KDEConduitConnection;
import org.kde.jenkins.oauth.api.KDEConduitUser;

import java.util.Arrays;
import java.util.Map;
import java.util.logging.Logger;

public class KDEAuthenticationToken extends AbstractAuthenticationToken {
    private KDEConduitUser user;

    public KDEAuthenticationToken(String accessToken, String conduitCertificate, String conduitUser) throws ConduitException {
        super(null); // We override the authorities and will defer them to our User object.

        LOGGER.info("building token");
        KDEConduitConnection connection = new KDEConduitConnection(accessToken);
        user = KDEConduitUser.whoami(connection);
        LOGGER.info(user.phid);
        LOGGER.info(user.userName);

        setAuthenticated(user != null);

        // FIXME: this is so obscenely hideous.
        String url = "https://phabricator.kde.org";
        Conduit conduit = ConduitFactory.createConduit(url, conduitUser, conduitCertificate);
        ProjectModule project = conduit.getProjectModule();
        ProjectModule.QueryResult queryResult = null;
        // The conduit API suffers from weird timeout problems. To mitigate problems from this we'll retry
        // the request multiple times before finally throwing it up and failing the authentication.
        int tryCount = 0;
        int maxTries = 4;
        while(true) {
            try {
                queryResult = project.query(
                        null, /* ids */
                        null, /* names */
                        null, /* phids */
                        null, /* slugs */
                        null, /* icons */
                        null, /* colors */
                        null, /* status */
                        Arrays.asList(user.phid), /* members */
                        null, /* limit */
                        null /* offset */
                );
                LOGGER.info(queryResult.toString());
                break;
            } catch (ConduitException e) {
                if (++tryCount >= maxTries) {
                    throw e;
                }
            }
        }


        Map<String, ProjectModule.ProjectResult> data = queryResult.getData();
        for (Map.Entry<String, ProjectModule.ProjectResult> entry : data.entrySet()) {
            ProjectModule.ProjectResult result = entry.getValue();
            LOGGER.info(result.toString());
            user.addAuthority(new GrantedAuthorityImpl(result.getPhid()));
        }
    }

    public KDEConduitUser getUser() {
        return user;
    }

    @Override
    public GrantedAuthority[] getAuthorities() {
        return user != null ? user.getAuthorities() : new GrantedAuthority[0];
    }

    @Override
    public Object getCredentials() {
        return StringUtils.EMPTY;
    }

    @Override
    public Object getPrincipal() {
        return getName();
    }

    @Override
    public String getName() {
        return (user != null ? user.getUsername() : null);
    }

    private static final Logger LOGGER = Logger.getLogger(KDEAuthenticationToken.class.getName());
}
