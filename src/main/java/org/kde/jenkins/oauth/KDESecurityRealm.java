 /*
     Copyright (C) 2016 Harald Sitter <sitter@kde.org>

     Licensed under the Apache License, Version 2.0 (the "License");
     you may not use this file except in compliance with the License.
     You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

     Unless required by applicable law or agreed to in writing, software
     distributed under the License is distributed on an "AS IS" BASIS,
     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
     See the License for the specific language governing permissions and
     limitations under the License.
 */

package org.kde.jenkins.oauth;

 import at.quelltextlich.phabricator.conduit.ConduitException;
 import com.thoughtworks.xstream.converters.ConversionException;
 import com.thoughtworks.xstream.converters.Converter;
 import com.thoughtworks.xstream.converters.MarshallingContext;
 import com.thoughtworks.xstream.converters.UnmarshallingContext;
 import com.thoughtworks.xstream.io.HierarchicalStreamReader;
 import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
 import hudson.Extension;
 import hudson.Util;
 import hudson.model.Descriptor;
 import hudson.model.User;
 import hudson.security.GroupDetails;
 import hudson.security.SecurityRealm;
 import hudson.security.UserMayOrMayNotExistException;
 import hudson.tasks.Mailer;
 import jenkins.model.Jenkins;
 import jenkins.security.SecurityListener;
 import org.acegisecurity.*;
 import org.acegisecurity.context.SecurityContextHolder;
 import org.acegisecurity.userdetails.UserDetails;
 import org.acegisecurity.userdetails.UserDetailsService;
 import org.acegisecurity.userdetails.UsernameNotFoundException;
 import org.apache.oltu.oauth2.client.OAuthClient;
 import org.apache.oltu.oauth2.client.URLConnectionClient;
 import org.apache.oltu.oauth2.client.request.OAuthClientRequest;
 import org.apache.oltu.oauth2.client.response.OAuthAuthzResponse;
 import org.apache.oltu.oauth2.client.response.OAuthJSONAccessTokenResponse;
 import org.apache.oltu.oauth2.common.exception.OAuthProblemException;
 import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
 import org.apache.oltu.oauth2.common.message.types.GrantType;
 import org.kohsuke.stapler.*;

 import java.io.IOException;
 import java.net.URI;
 import java.net.URISyntaxException;
 import java.util.logging.Logger;

public class KDESecurityRealm extends SecurityRealm implements UserDetailsService {
    private static final String DEFAULT_AUTH_URI = "https://phabricator.kde.org/oauthserver/auth/";
    private static final String DEFAULT_TOKEN_URI = "https://phabricator.kde.org/oauthserver/token/";
    private static final String DEFAULT_OAUTH_SCOPES = "whoami";

    private String clientID;
    private String clientSecret;
    private String conduitCertificate;
    private String conduitUser;

    @DataBoundConstructor
    public KDESecurityRealm(
            String clientID,
            String clientSecret,
            String conduitCertificate,
            String conduitUser) {
        super();

        this.clientID     = Util.fixEmptyAndTrim(clientID);
        this.clientSecret = Util.fixEmptyAndTrim(clientSecret);
        this.conduitCertificate  = Util.fixEmptyAndTrim(conduitCertificate);
        this.conduitUser  = Util.fixEmptyAndTrim(conduitUser);
    }

    private KDESecurityRealm() { }

    private String getRedirectURI() {
        try {
            String rootUrl = Jenkins.getInstance().getRootUrl();
            String uri = rootUrl + "/securityRealm/finishLogin";
            uri = new URI(uri).normalize().toString();
            return uri;
        } catch (URISyntaxException e) {
        }
        return null;
    }

    private void setClientID(String clientID) {
        this.clientID = clientID;
    }

    private void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    public String getClientID() {
        return clientID;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public String getConduitCertificate() {
        return conduitCertificate;
    }

    public void setConduitCertificate(String conduitCertificate) {
        this.conduitCertificate = conduitCertificate;
    }

    public String getConduitUser() {
        return conduitUser;
    }

    public void setConduitUser(String conduitUser) {
        this.conduitUser = conduitUser;
    }

    public HttpResponse doCommenceLogin(StaplerRequest request, @Header("Referer") final String referer)
            throws IOException, OAuthSystemException {
        // Set the referer on our request, upon finish this allows us to
        // redirect back to the page where the user clicked or was prompted to
        // log in.
        request.getSession().setAttribute(REFERER_ATTRIBUTE,referer);

        // Required - client_id - the id of the newly registered client.
        // Required - response_type - the desired type of authorization code response. Only code is supported at this time.
        // Optional - redirect_uri - override the redirect_uri the client registered. This redirect_uri must have the same fully-qualified domain, path, port and have at least the same query parameters as the redirect_uri the client registered, as well as have no fragments.
        // Optional - scope - specify what scope(s) the client needs access to in a space-delimited list.
        // Optional - state - an opaque value the client can send to the server for programmatic excellence. Some clients use this value to implement XSRF protection or for debugging purposes.

        OAuthClientRequest oauthRequest = OAuthClientRequest
             .authorizationLocation(DEFAULT_AUTH_URI)
             .setClientId(clientID)
             .setResponseType("code")
             .setRedirectURI(getRedirectURI())
             .setScope(DEFAULT_OAUTH_SCOPES)
             .buildQueryMessage();

        return new HttpRedirect(oauthRequest.getLocationUri());
    }

    public HttpResponse doFinishLogin(StaplerRequest stabler_request)
            throws IOException, OAuthProblemException, OAuthSystemException, ConduitException {
        OAuthAuthzResponse oar = OAuthAuthzResponse.oauthCodeAuthzResponse(stabler_request);
        String code = oar.getCode();

        if (code == null || code.trim().length() == 0) {
            LOGGER.info("doFinishLogin: missing code.");
            return HttpResponses.redirectToContextRoot();
        }

        // Required - client_id - the id of the client
        // Required - client_secret - the secret of the client. This is used to authenticate the client.
        // Required - code - the authorization code obtained earlier.
        // Required - grant_type - the desired type of access grant. Only token is supported at this time.
        // Optional - redirect_uri - should be the exact same redirect_uri as the redirect_uri specified to obtain the authorization code. If no redirect_uri was specified to obtain the authorization code then this should not be specified.

        OAuthClientRequest request = OAuthClientRequest
            .tokenLocation(DEFAULT_TOKEN_URI)
            .setClientId(clientID)
            .setClientSecret(clientSecret)
            .setCode(code)
            .setGrantType(GrantType.AUTHORIZATION_CODE)
            .setRedirectURI(getRedirectURI())
            .buildQueryMessage();

        OAuthClient oAuthClient = new OAuthClient(new URLConnectionClient());
        OAuthJSONAccessTokenResponse oAuthResponse = oAuthClient.accessToken(request);
        String accessToken = oAuthResponse.getAccessToken();

        if (accessToken != null && accessToken.trim().length() > 0) {
            KDEAuthenticationToken auth = new KDEAuthenticationToken(
                accessToken,
                conduitCertificate,
                conduitUser
            );
            SecurityContextHolder.getContext().setAuthentication(auth);

            User u = User.current();
            u.setFullName(auth.getUser().realName); // FIXME: should happen in token, no?
            if (!u.getProperty(Mailer.UserProperty.class).hasExplicitlyConfiguredAddress()) {
                if (auth.getUser().primaryEmail != null) {
                    u.addProperty(new Mailer.UserProperty(auth.getUser().primaryEmail));
                }
            }

            SecurityListener.fireAuthenticated(auth.getUser());
        } else {
            LOGGER.info("received no access token...");
        }

        String referer = (String)stabler_request.getSession().getAttribute(REFERER_ATTRIBUTE);
        if (referer!=null) {
            return HttpResponses.redirectTo(referer);
        }
        return HttpResponses.redirectToContextRoot();
    }

    @Override
    public boolean allowsSignup() {
        return false;
    }

    @Override
    public SecurityComponents createSecurityComponents() {
        return new SecurityComponents(new AuthenticationManager() {
            public Authentication authenticate(Authentication authentication) throws AuthenticationException {
                if (authentication instanceof KDEAuthenticationToken) {
                    return authentication;
                }
                throw new BadCredentialsException("unexpected type: " + authentication);
            }
        }, new UserDetailsService() {
            public UserDetails loadUserByUsername(String username)
                    throws UsernameNotFoundException {
                return KDESecurityRealm.this.loadUserByUsername(username);
            }
        });
    }

    @Override
    public String getLoginUrl() {
        return "securityRealm/commenceLogin";
    }

    @Override
    public DescriptorImpl getDescriptor() {
        return (DescriptorImpl) super.getDescriptor();
    }

    @Override
    public UserDetails loadUserByUsername(String username)
            throws UsernameNotFoundException {
        throw new UserMayOrMayNotExistException("KDE security realm does not implement username lookup");
    }

    @Override
    public boolean equals(Object object){
        if(object instanceof KDESecurityRealm) {
            KDESecurityRealm obj = (KDESecurityRealm) object;
            return this.getClientID().equals(obj.getClientID()) &&
                this.getClientSecret().equals(obj.getClientSecret()) &&
                this.getConduitCertificate().equals(obj.getConduitCertificate()) &&
                this.getConduitUser().equals(obj.getConduitUser());
        } else {
            return false;
        }
    }

    public static final class ConverterImpl implements Converter {
        public boolean canConvert(Class type) {
            return type == KDESecurityRealm.class;
        }

        public void marshal(Object source, HierarchicalStreamWriter writer,
                MarshallingContext context) {
            KDESecurityRealm realm = (KDESecurityRealm) source;

            writer.startNode("clientID");
            writer.setValue(realm.getClientID());
            writer.endNode();

            writer.startNode("clientSecret");
            writer.setValue(realm.getClientSecret());
            writer.endNode();

            writer.startNode("conduitCertificate");
            writer.setValue(realm.getConduitCertificate());
            writer.endNode();

            writer.startNode("conduitUser");
            writer.setValue(realm.getConduitUser());
            writer.endNode();
        }

        public Object unmarshal(HierarchicalStreamReader reader,
                UnmarshallingContext context) {
            KDESecurityRealm realm = new KDESecurityRealm();

            String node;
            String value;

            while (reader.hasMoreChildren()) {
                reader.moveDown();
                node = reader.getNodeName();
                value = reader.getValue();
                setValue(realm, node, value);
                reader.moveUp();
            }

            return realm;
        }

        private void setValue(KDESecurityRealm realm, String node,
                String value) {
            if (node.equalsIgnoreCase("clientid")) {
                realm.setClientID(value);
            } else if (node.equalsIgnoreCase("clientsecret")) {
                realm.setClientSecret(value);
            } else if (node.equalsIgnoreCase("conduitcertificate")) {
                realm.setConduitCertificate(value);
            } else if (node.equalsIgnoreCase("conduituser")) {
                realm.setConduitUser(value);
            } else {
                throw new ConversionException("Invalid node value = " + node);
            }
        }
    }

    @Extension
    public static final class DescriptorImpl extends Descriptor<SecurityRealm> {
        @Override
        public String getHelpFile() {
            return "/plugin/kde-oauth-plugin/help/help-security-realm.html";
        }

        @Override
        public String getDisplayName() {
            return "KDE Authentication";
        }

        public DescriptorImpl() {
            super();
        }

        public DescriptorImpl(Class<? extends SecurityRealm> clazz) {
            super(clazz);
        }
    }

    private static final Logger LOGGER = Logger.getLogger(KDESecurityRealm.class.getName());
    private static final String REFERER_ATTRIBUTE = KDESecurityRealm.class.getName()+".referer";
}
