/*
    Copyright (C) 2016 Harald Sitter <sitter@kde.org>

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

package org.kde.jenkins.oauth.api;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import hudson.security.SecurityRealm;
import org.acegisecurity.GrantedAuthority;
import org.acegisecurity.userdetails.UserDetails;
import org.apache.oltu.oauth2.client.response.OAuthResourceResponse;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class KDEConduitUser implements UserDetails {
    public class Response {
        public KDEConduitUser result;
        public Integer error_code;
        public String error_info;
    }

    @SerializedName("phid")
    public String phid;
    @SerializedName("userName")
    public String userName;
    @SerializedName("realName")
    public String realName;
    @SerializedName("image")
    public String image;
    @SerializedName("uri")
    public String uri;
    @SerializedName("roles")
    public List<String> roles;
    @SerializedName("primaryEmail")
    public String primaryEmail;

    private final List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();

    public KDEConduitConnection connection;

    private KDEConduitUser() {
        super();
        authorities.add(SecurityRealm.AUTHENTICATED_AUTHORITY);
    }

    public static KDEConduitUser whoami(KDEConduitConnection connection) {
        OAuthResourceResponse response = connection.get("/user.whoami");
        Gson gson = new Gson();
        Response self_response = gson.fromJson(response.getBody(), Response.class);
        if (self_response != null) {
            return self_response.result;
        }
        return null;
    }

    public void addAuthority(GrantedAuthority authority) {
        authorities.add(authority);
    }

    @Override
    public GrantedAuthority[] getAuthorities() {
        return authorities.toArray(new GrantedAuthority[authorities.size()]);
    }

    @Override
    public String getPassword() {
        return null;
    }

    @Override
    public String getUsername() {
        return this.userName;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    private static final Logger LOGGER = Logger.getLogger(KDEConduitUser.class.getName());
}
