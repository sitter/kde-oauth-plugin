/*
    Copyright (C) 2016 Harald Sitter <sitter@kde.org>

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

package org.kde.jenkins.oauth.api;

import org.apache.oltu.oauth2.client.OAuthClient;
import org.apache.oltu.oauth2.client.URLConnectionClient;
import org.apache.oltu.oauth2.client.request.OAuthBearerClientRequest;
import org.apache.oltu.oauth2.client.request.OAuthClientRequest;
import org.apache.oltu.oauth2.client.response.OAuthResourceResponse;
import org.apache.oltu.oauth2.common.OAuth;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;

import java.util.logging.Logger;

public class KDEConduitConnection {
    private String accessToken;
    private String hostURI;
    private OAuthClient oAuthClient;

    public KDEConduitConnection(String accessToken) {
        this.accessToken = accessToken;
        this.hostURI = "https://phabricator.kde.org/api";
        this.oAuthClient = new OAuthClient(new URLConnectionClient());
    }

    public OAuthClientRequest request(String path) throws OAuthSystemException {
        LOGGER.info(hostURI + path);
        return new OAuthBearerClientRequest(hostURI + path)
            .setAccessToken(accessToken)
            .buildQueryMessage();
    }

    public OAuthResourceResponse get(String path) {
        try {
            OAuthClientRequest bearerClientRequest = request(path);
            return oAuthClient.resource(bearerClientRequest, OAuth.HttpMethod.GET, OAuthResourceResponse.class);
        } catch (Exception e) {}
        return null;
    }

    private static final Logger LOGGER = Logger.getLogger(KDEConduitConnection.class.getName());
}
